# This script is meant for building the project in a local environment already installed with gcloud, terraform, helm and kubectl

cd terraform
# build gke cluster
terraform init && terraform apply -auto-approve -input=false
K8s_CLUSTER_NAME=$(terraform output --raw kubernetes_cluster_name)
GCLOUD_REGION=$(terraform output --raw region)
GCLOUD_PROJECT_ID=$(terraform output --raw project_id)
# configure gcloud, .gcloud-api-key.json is gcloud credential
gcloud auth activate-service-account --key-file .gcloud-api-key.json
# configure kubectl
gcloud container clusters get-credentials $K8s_CLUSTER_NAME --region=$GCLOUD_REGION --project $GCLOUD_PROJECT_ID
cd -
# build application and push to gcp docker image registry
DOCKER_GCR_REPO_URL=gcr.io
DOCKER_IMAGE_NAME=resume
docker login -u _json_key --password-stdin https://$DOCKER_GCR_REPO_URL < .gcloud-api-key.json
REPO_URL=($DOCKER_GCR_REPO_URL/$GCLOUD_PROJECT_ID/$DOCKER_IMAGE_NAME) && echo $REPO_URL
docker build -t "$REPO_URL:latest" .
docker push "$REPO_URL:latest"
# deploy application
cd terraform/k8s
terraform init && terraform apply -auto-approve -input=false
