from flask import Flask, request, abort
from flask import render_template
from application import application
import sys


@application.route('/cv')
def resume():
    return render_template('resume.html')


@application.route('/tabletennis')
def tabletennies():
    return render_template('tabletennies.html')


@application.route('/plane')
def plane():
    return render_template('plane.html')


@application.route('/checkbox')
def checkbox():
    return render_template('checkbox.html')


@application.route('/chat')
def chat():
    return render_template('chat.html')


@application.route('/')
def future():
    return render_template('future.html')


@application.route('/blocks')
def blocks():
    return render_template('blocks.html')


@application.route('/robots')
def robots():
    return render_template('robots.html')


@application.route('/tunnel')
def tunnel():
    return render_template('tunnel.html')


@application.route('/ai-hand')
def aihand():
    return render_template('ai-hand.html')


if __name__ == "__main__":
    application.debug = True
    application.run(host='0.0.0.0')
