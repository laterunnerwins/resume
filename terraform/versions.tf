terraform {
  backend "gcs" {
    bucket  = "tf-state-gke-cluster"
    prefix  = "terraform/state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.63.0"
    }
  }

  required_version = "~> 0.14"
}