resource "kubernetes_deployment" "resume_deployment" {
  metadata {
    name = "resume-deployment"
    labels = {
      app = "resume"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "resume"
      }
    }

    template {
      metadata {
        labels = {
          app = "resume"
        }
      }

      spec {
        container {
          image = "gcr.io/gke-cluster-307219/resume:latest"
          name  = "resume"
          
          port {
            container_port = 5000
          }
          
          command = ["python3"]
          args = ["application.py"]

          resources {
            limits = {
              cpu    = "0.25"
              memory = "512Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 5000
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "resume_service" {
  metadata {
    name = "resume-service"
  }
  spec {
    selector = {
      app = kubernetes_deployment.resume_deployment.metadata.0.labels.app
    }
    port {
      port = 80
      target_port = 5000
      protocol = "TCP"
    }
    type = "NodePort"
  }
}

resource "kubernetes_ingress" "resume_ingress" {
  depends_on = [
    helm_release.ingress-nginx
  ]
  wait_for_load_balancer = true
  metadata {
    name = "resume-ingress"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      host = "cv.deepvision.sg"
      http {
        path {
          path = "/"
          backend {
            service_name = kubernetes_service.resume_service.metadata.0.name
            service_port = 80
          }
        }
      }
    }
  }
}

# Display load balancer IP (typically present in GCP, or using Nginx ingress controller)
output "load_balancer_ip" {
  value = kubernetes_ingress.resume_ingress.status.0.load_balancer.0.ingress.0.ip
}