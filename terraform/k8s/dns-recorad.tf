data "cloudflare_zone" "zone" {
  name = var.zone
}

variable "zone" {
  description = "zone"
}

variable "hosts" {
  description = "hosts"
  type = list
}

resource "cloudflare_record" "sites" {
  count = length(var.hosts)
  depends_on = [
    kubernetes_ingress.resume_ingress
  ]
  zone_id = data.cloudflare_zone.zone.id
  name    = var.hosts[count.index]
  value   = kubernetes_ingress.resume_ingress.status.0.load_balancer.0.ingress.0.ip
  type    = "A"
  proxied = true
}