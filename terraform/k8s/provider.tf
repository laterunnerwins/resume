terraform {
  backend "gcs" {
    bucket  = "tf-state-gke-cluster"
    prefix  = "terraform/k8s-state"
  }
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "gke_gke-cluster-307219_asia-southeast1_gke-cluster-307219-gke"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
    config_context = "gke_gke-cluster-307219_asia-southeast1_gke-cluster-307219-gke"
  }
}

provider "cloudflare" {
}