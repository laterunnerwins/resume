FROM python:3.8-alpine3.13 as base
FROM base as builder
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "application.py"]