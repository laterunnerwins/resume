# This script is meant for destruct the project in a local environment already installed with gcloud, terraform, helm and kubectl

cd terraform/k8s
# build gke cluster
terraform init && terraform destroy -auto-approve -input=false
cd ..
terraform init && terraform destroy -auto-approve -input=false